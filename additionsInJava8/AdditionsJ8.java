import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;
import java.util.Collections;
import java.util.Map;
import java.util.HashMap;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.function.BiFunction;

public class AdditionsJ8{
	
	public static void main(String[] args){
		mappy();
	}
	
	public static void consi(){
		//Static method reference
		Consumer<List<Integer>> methodRef1 = Collections::sort;
		Consumer<List<Integer>> lambda1 = l -> Collections.sort(l);
		List<Integer> list = new ArrayList<>();
		list.add(1);
		list.add(7);
		list.add(3);
		methodRef1.accept(list);
		System.out.println(list);
	}
	
	public static void prediInce(){
		//Instance method on a specific instance
		String str = "abc";
		Predicate<String> methodRef2 = str::startsWith;
		Predicate<String> lambda2 = s -> str.startsWith(s);
		System.out.println(lambda2.test("a"));
		System.out.println(methodRef2.test("b"));
	}
	
	public static void predi(){
		//Instance method without knowing the istance in advance
		Predicate<String> methodRef3 = String::isEmpty;
		Predicate<String> lambda3 = s -> s.isEmpty();
		System.out.println("Is empty? " + methodRef3.test("abc"));
	}
	
	public static void supli(){
		//Constructor reference
		Supplier<ArrayList<String>> methodRef4 = ArrayList::new;
		Supplier<ArrayList<String>> lambda4 = () -> new ArrayList<>(); 
		//adding <String> here ^ with <>			here ^
		//satisifed unchecked conversion below
		List<String> strings = methodRef4.get();
		strings.add("word");
		System.out.println(strings);
	}
	
	public static void rmvCond(){
		List<String> list = new ArrayList<>();
		list.add("Magician");
		list.add("Assistant");
		System.out.println(list);
		list.removeIf(s -> s.startsWith("A"));
		System.out.println(list);
		Predicate<List<String>> methodRef5 = List::isEmpty;
		System.out.println(methodRef5.test(list));
	}
	
	public static void upAll(){
		List<Integer> list = Arrays.asList(1, 2, 3);
		list.replaceAll(x -> x*x);
		System.out.println(list);
	}
	
	public static void catsLoop(){
		List<String> cats = Arrays.asList("Burek", "Kocurek");
		for(String cat: cats) System.out.println(cat);
		cats.forEach(c -> System.out.println(c));
		cats.forEach(System.out::println);
	}
	
	public static String cep(){
		try{
			System.out.println("I have tried.");
			return "try";
			//funny story above - throw and return cannot be there together,
			//because one of them is unreachable		
		}catch(Exception e){
			System.out.println("I got an exception.");
			return "catch";
		}finally{
			System.out.println("Finally it is the end.");
			return "finally";
		}
	}
	
	public static void mappy(){
		Map<String, String> fav = new HashMap<>();
		fav.put("Jenny", "Bus Tour");
		fav.put("Jenny", "Tram");
		System.out.println(fav);
		
		fav.put("Tom", null);
		
		fav.putIfAbsent("Jenny", "is ready");
		fav.putIfAbsent("Sam", "Car");
		fav.putIfAbsent("Tom", "Airplane");
		System.out.println(fav);
			
		BiFunction<String, String, String> mapper = (v1, v2) 
			-> v1.length() > v2.length() ? v1 : v2;
			
		fav.merge("Jenny", "Bus", mapper);
		fav.merge("Sam", "Supercar", mapper);
		System.out.println(fav);
	}
	
	
}
