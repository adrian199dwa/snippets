import java.util.*;

//The Diamond Operator

class Doggies{
	List<String> names;
	Doggies(){
		names = new ArrayList<>();		//matches instance variable
	}
	
	public void copyNames(){
		ArrayList<String> copyOfNames;
		copyOfNames = new ArrayList<>(); 	//mataches local variable
	}
}
