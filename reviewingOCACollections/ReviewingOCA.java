import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;
import java.util.Collections;

public class ReviewingOCA{

	public static void main(String[] args){
		page_107();
	}

	//Array and ArrayList
	
	public static void page_104(){
		List<String> list = new ArrayList<>();
		list.add("Fluffy");
		list.add("Webby");
		
		String[] array = new String[list.size()];
		array[0] = list.get(1);
		array[1] = list.get(0);
		
		for(int i = 0; i< array.length; i++) System.out.print(array[i] + "-");
	}
	
	public static void page_105(){
		String[] array = {"gerbil", "mouse"};
		System.out.println(Arrays.toString(array));
		
		List<String> list = Arrays.asList(array); 	//backed fixed size list
		System.out.println(list);
		
		list.set(1, "test"); 				//changes both list and array
		System.out.println(Arrays.toString(array));
		System.out.println(list);
		
		array[0] = "new";
		System.out.println(Arrays.toString(array));
		System.out.println(list);
		
		//array[2] = "2"; //java.lang.ArrayIndexOutOfBoundsException
		
		//list.add("2"); //java.lang.UnsupportedOperationException

		String[] array2 = list.toArray(new String[0]);
		
		array2[0] = null;
		System.out.println(Arrays.toString(array));
		System.out.println(list);
		System.out.println(Arrays.toString(array2));
	}
	
	//Searchning and Sorting

	public static void page_105_2(){
		int[] numbers = {6, 9, 1, 8, 11};
		Arrays.sort(numbers);
		System.out.println(Arrays.binarySearch(numbers,6));
		System.out.println(Arrays.binarySearch(numbers,3));
	}
	
	public static void page_106(){
		List<Integer> list = Arrays.asList(6, 9, 1, 8, 11);
		Collections.sort(list);
		System.out.println(Collections.binarySearch(list,9));
	}
	
	//Wrapper Classes and Autoboxing
	
	public static void page_107(){
		List<Integer> numbers = new ArrayList<>();
		numbers.add(1);
		numbers.add(new Integer(5));
		numbers.remove(1);
		System.out.println(numbers);
	}
}

