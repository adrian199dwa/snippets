//Generic Classes

public class Crate<T>{
	
	public static void main(String[] args){
		Elephant ele = new Elephant();
		Crate<Elephant> eleInCrate = new Crate<>();
		eleInCrate.packCrate(ele);
		Elephant inNewHome = eleInCrate.emptyCrate();
		Crate<Elephant> eleInCrateForShip = Crate.<Elephant>ship(inNewHome);
		//... = ship(inNewHome); 		//works as well
	}
	
	private T contents;
	
	public T emptyCrate(){
		return contents;
	}
	
	public void packCrate(T contents){
		this.contents = contents;
	}
	
	public static <T> Crate<T> ship(T t){		//formal parameter type is required
		System.out.println("Preparing " + t);
		Crate<T> crate = new Crate<T>();
		crate.packCrate(t);
		return crate;
	}
}

//Type erasure during compilation- <T> disappears and T becomes Object and cast is added:
//		Elephant inNewHome = (Elephant) eleInCrate.emptyCrate();
