import java.util.ArrayList;
import java.util.List;

public class Bounds{
	
	public static void main(String[] args){
		List<String> keywords = new ArrayList<>();
		keywords.add("java");
		keywords.add("tava");
		
		//printList(keywords);	//does not compile 
		//we cannot assign List<String> to List<Object> see note OCP 118
		
		printListBounds(keywords);
		
		List<Integer> numbers = new ArrayList<>();
		numbers.add(5);
		numbers.add(46);
		
		printListBounds(numbers);
		
		printListGenerics2(numbers);
		
		page_117();
	}
	
	public static void page_117(){
		List<?> l = new ArrayList<String>();
		List<? extends Object> l2 = new ArrayList<String>();
		List<? super String> l3 = new ArrayList<String>();
		
		String str = "abc";
		
		//unbouned and upper bounded list are logically immutable 
		//l.add(str);
		//l2.add(new Object());
		//l2.add(str);
		l3.add(str);
	}
	
	public static void printList(List<Object> list){	//it does not help us
		for(Object x : list) System.out.println(x);
	}
	
	
	
	public static void printListBounds(List<?> list){	//it does what we want
		for(Object x : list) System.out.println(x);
	}
	
	/*public static <T> void printListGenerics(T t){
		for(Object x : t) System.out.println(x);	//not java.lang.Iterable
	}*/
	
	public static <T> void printListGenerics2(List<T> list){
		for(Object x : list) System.out.println(x);
	}
	
	public static void page_121(){
		List<String> strings = new ArrayList<String>();
		strings.add("tweet");
		List<Object> objects = new ArrayList<Objects>(strings);
		addSound(strings);
		addSound(objects);
	}
	
	public static void addSound(List<? super String> list){
		list.add("quack"):
	}
	
}
