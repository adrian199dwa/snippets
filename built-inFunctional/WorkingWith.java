import java.util.function.Supplier;
import java.util.function.Consumer;
import java.util.function.BiConsumer;
import java.util.function.Predicate;
import java.util.function.BiPredicate;
import java.util.function.Function;
import java.util.function.BiFunction;
import java.time.*;
import java.util.Map;
import java.util.HashMap;

public class WorkingWith{

	public static void main(String[] args){
		fun();
	}
	
	//implementing supplier
	static void supp(){
		Supplier<LocalDate> s1 = LocalDate::now;
		Supplier<LocalDate> s2 = () -> LocalDate.now();
		
		LocalDate d1 = s1.get();
		LocalDate d2 = s2.get();
		
		System.out.println(d1);
	}
	
	//implementing consumer and biConsumer
	static void con(){
		Consumer<String> c1 = System.out::println;
		c1.accept("cepted");
		
		Map<String, Integer> map = new HashMap<>();
		BiConsumer<String, Integer> b1 = map::put;
		BiConsumer<String, Integer> b2 = (k, v) -> map.put(k, v);
		
		b1.accept("chicken", 7);
		b2.accept("chick", 1);
		
		System.out.println(map);
	}
	
	//implementing predicata and bipredicate
	static void pred(){
		Predicate<String> p1 = String::isEmpty;
		Predicate<String> p2 = x -> x.isEmpty();
		
		sop(p1.test(""));
		sop(p2.test("abc"));
		
		BiPredicate<String, String> b1 = String::startsWith;
		BiPredicate<String, String> b2 = (str, prefix) -> str.startsWith(prefix);
		
		sop(b1.test("abc","ab"));
		sop(b2.test("bca", "g"));
	}
	
	static void sop(Object t){
		System.out.println(t);
	}
	
	//implementing function and bifunction
	static void fun(){
		Function<String, Integer> f1 = String::length;
		Function<String, Integer> f2 = x -> x.length();
		
		sop(f1.apply("better go to sleep"));
		sop(f2.apply("better code more"));
		
		BiFunction<String, String, String> b1 = String::concat;
		BiFunction<String, String, String> b2 = (string, toAdd) -> string.concat(toAdd);
		
		sop(b1.apply("str1", "str2"));
	}
}
