import java.util.Comparator;

public class ChainingComparator implements Comparator<Squirrel>{
	public int compare(Squirrel s1, Squirrel s2){
		Comparator<Squirrel> c = Comparator.comparing(s -> s.getSpecies());
		c = c.thenComparingInt(s -> s.getWeight());
		return c.compare(s1, s2);	//is not the same compare as above
		//above is ChainingComparator.compare(s1, s2) here is Comparator.compare(s1, s2)
	}
}
