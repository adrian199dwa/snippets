import java.util.*;

public class Duck implements Comparable<Duck>{

	private String name;
	private int weight;
	
	public Duck(String name, int weight){
		this.name = name;
		this.weight = weight;
	}
	
	public String toString(){ return name; }
	public String getName(){ return name; }
	public int getWeight(){ return weight; }
	
	public int compareTo(Duck d){ return name.compareTo(d.name); }
	
	public static void main(String[] args){
		List<Duck> ducks = new ArrayList<>();
		ducks.add(new Duck("Quack", 7));
		ducks.add(new Duck("Puddles", 2));
		ducks.add(new Duck("Ansible", 12));
		Collections.sort(ducks);
		System.out.println(ducks);
		
		Comparator<Duck> byWeightLambda = (d1, d2) -> d1.getWeight() - d2.getWeight();
		
		Comparator<Duck> byWeight = new Comparator<Duck>(){
				//anonymous inner class
				public int compare(Duck d1, Duck d2){
					return d1.getWeight()-d2.getWeight();
				}
			};
		
		Collections.sort(ducks, byWeightLambda);
		System.out.println(ducks);
	}
}
