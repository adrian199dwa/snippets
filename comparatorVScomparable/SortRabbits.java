import java.util.*;
public class SortRabbits{
	static class Rabbit{ 
		int id; 
		public String toString(){ return String.valueOf(id); }
		public Rabbit(int id){ this.id = id; }
	}
	public static void main(String[] args){
		List<Rabbit> rabbits = new ArrayList<>();
		rabbits.add(new Rabbit(1));
		rabbits.add(new Rabbit(5));
		rabbits.add(new Rabbit(2));
		rabbits.add(new Rabbit(0));
		Comparator<Rabbit> c = (r1, r2) -> r1.id - r2.id;
		/*Comparator<Rabbit> c = new Comparator<Rabbit>(){
			public int compare(Rabbit r1, Rabbit r2){
				return r1.id - r2.id;
			}
		}*/
		Collections.sort(rabbits, c);
		System.out.println(rabbits);
	
	}
}
