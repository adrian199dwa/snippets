public class Squirrel{
	private int weight;
	private String species;
	public Squirrel(String theSpecies){
		if(theSpecies == null) throw new IllegalArgumentException();
		species = theSpecies;
	}
	public int getWeight(){return weight;}
	public void setWeight(int weight){this.weight = weight;}
	public String getSpecies(){return species;}
	
	public static void main(String[] args){
		Squirrel s1 = new Squirrel("red");
		Squirrel s2 = new Squirrel("red");
		
		s1.setWeight(10);
		s2.setWeight(21);
		
		MultiFieldComparator comp = new MultiFieldComparator();
		System.out.println(comp.compare(s1, s2));
		
		ChainingComparator chain = new ChainingComparator();
		System.out.println(chain.compare(s1,s2));
	}
}
